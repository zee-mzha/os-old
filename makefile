x86:
	nasm -felf32 arch/x86/boot.asm -o boot.o -Iarch/x86/
	nasm -felf32 arch/x86/idt.asm -o idt.o -Iarch/x86/
	nasm -felf32 arch/x86/event.asm -o event.o -Iarch/x86/
	i686-elf-gcc -c */*.c *.c -std=gnu99 -ffreestanding -O2 -Wall -Wextra -Ilibc/
	i686-elf-gcc -T linker.ld -o myos.bin -ffreestanding -O2 -nostdlib *.o -lgcc
	mkdir -p isodir/boot/grub
	cp myos.bin isodir/boot/
	cp grub.cfg isodir/boot/grub/
	grub2-mkrescue -o myos86.iso isodir
	rm *.o myos.bin isodir -r

x86_64:
	nasm -felf64 arch/x86_64/boot.asm -o boot.o -Iarch/x86_64/
	nasm -felf64 arch/x86_64/idt.asm -o idt.o -Iarch/x86_64/
	nasm -felf64 arch/x86_64/event.asm -o event.o -Iarch/x86_64/
	x86_64-elf-gcc -c */*.c *.c -std=gnu99 -ffreestanding -O2 -Wall -Wextra -Ilibc/
	x86_64-elf-gcc -T linker.ld -o myo.bin -ffreestanding -O2 -nostdlib *.o -lgcc
	mkdir -p isodir/boot/grub
	cp myos.bin isodir/boot/
	cp grub.cfg isodir/boot/grub/
	grub2-mkrescue -o myos86_64.iso isodir
	rm *.o myos.bin isodir -r

clean:
	rm *.o myos.bin isodir -r
