#ifndef __IO_H__
#define __IO_H__

#include <stdint.h>

#define VGA_WIDTH 80
#define VGA_HEIGHT 25

void outb(uint16_t port, uint8_t value);
uint8_t inb(uint16_t port);

#endif /*__IO_H__*/
