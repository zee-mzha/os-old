#include "terminal.h"

uint8_t terminal_color = (uint8_t) VGA_COLOR_LIGHT_GREEN | (uint8_t) VGA_COLOR_BLACK << 4;
uint16_t cursor_pos = 0;
uint16_t *terminal_buffer = (uint16_t*) 0xB8000;

void terminal_putchar(char c){
	if(cursor_pos >= VGA_WIDTH*VGA_HEIGHT){
		for(uint16_t i = 0; i < VGA_WIDTH*(VGA_HEIGHT-1); i++){
			terminal_buffer[i] = terminal_buffer[i+VGA_WIDTH];
		}
		cursor_pos = VGA_WIDTH*(VGA_HEIGHT-1);
	}
	switch(c){
		case '\n':
			cursor_pos -= cursor_pos%VGA_WIDTH;
			/*are we on the last line?*/
			if(cursor_pos == VGA_WIDTH*(VGA_HEIGHT-1)){
				for(uint16_t i = 0; i < VGA_WIDTH*(VGA_HEIGHT-1); i++){
					terminal_buffer[i] = terminal_buffer[i+VGA_WIDTH];
				}
			}
			cursor_pos += VGA_WIDTH;
			break;
		case '\r':
			cursor_pos -= cursor_pos%VGA_WIDTH;
			break;
		case '\t':
			for(unsigned int i = 0; i < TAB_SIZE; i++){
				terminal_putchar(' ');
			}
			break;
		case '\b':
			terminal_buffer[--cursor_pos] = (uint16_t) ' ' | (uint16_t) terminal_color << 8;
			break;
		default:
			terminal_buffer[cursor_pos++] = (uint16_t) c | (uint16_t) terminal_color << 8;
	}
	move_cursor(cursor_pos);
}

void terminal_puts(const char * str){
	size_t len = strlen(str);
	for(size_t i = 0; i < len; i++){
		terminal_putchar(str[i]);
	}
}

void move_cursor(uint16_t pos){
	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t) pos);
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) (pos >> 8));
}

void move_cursorXY(int x, int y){
	uint16_t pos = y*VGA_HEIGHT-x;
	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t) pos);
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) (pos >> 8));
}
