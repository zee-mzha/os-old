#ifndef __TERMINAL_H__
#define __TERMINAL_H__

#include <stddef.h>
#include <stdint.h>
#include "string.h"
#include "../io/io.h"

#define VGA_WIDTH 80
#define VGA_HEIGHT 25
#define TAB_SIZE 4

enum vga_color{
	VGA_COLOR_BLACK,
	VGA_COLOR_BLUE,
	VGA_COLOR_GREEN,
	VGA_COLOR_CYAN,
	VGA_COLOR_RED,
	VGA_COLOR_MAGENTA,
	VGA_COLOR_BROWN,
	VGA_COLOR_LIGHT_GREY,
	VGA_COLOR_DARK_GREY,
	VGA_COLOR_LIGHT_BLUE,
	VGA_COLOR_LIGHT_GREEN,
	VGA_COLOR_LIGHT_CYAN,
	VGA_COLOR_LIGHT_RED,
	VGA_COLOR_LIGHT_MAGENTA,
	VGA_COLOR_LIGHT_BROWN,
	VGA_COLOR_WHITE
};

extern uint8_t terminal_color;
extern uint16_t cursor_pos;
extern uint16_t *terminal_buffer;

void terminal_putchar(char c);
void terminal_puts(const char *str);

void move_cursor(uint16_t pos);
void move_cursorXY(int x, int y);

#endif /*__TERMINAL_H__*/
