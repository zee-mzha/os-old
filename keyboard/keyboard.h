#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <stdint.h>

#define KB_EVFUNC_COUNT 256

typedef struct scancode{
	char c;
	char shift;
	char caps;
} scancode;

extern scancode scancode_set1[];
extern char shift_state;
extern char caps_state;
extern void *kb_evfunc[KB_EVFUNC_COUNT];

int register_kb_evfunc(void *);
int unregister_kb_evfunc(void *);
void call_kb_evfunc(uint8_t scancode);
char get_value(char scancode);

#endif /*__KEYBOARD_H__*/
