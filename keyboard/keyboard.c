#include "keyboard.h"

scancode scancode_set1[] = {
	{'\0', '\0', '\0'}, /*reserved*/
	{'?', '?', '?'}, /*escape*/
	{'1', '!', '1'}, /*1*/
	{'2', '@', '2'}, /*2*/
	{'3', '#', '3'},
	{'4', '$', '4'},
	{'5', '%', '5'},
	{'6', '^', '6'},
	{'7', '&', '7'},
	{'8', '*', '8'},
	{'9', '(', '9'},
	{'0', ')', '0'},
	{'-', '_', '-'},
	{'=', '+', '='},
	{'\b', '\b', '\b'},
	{'\t', '\t', '\t'},
	{'q', 'Q', 'Q'},
	{'w', 'W', 'W'},
	{'e', 'E', 'E'},
	{'r', 'R', 'R'},
	{'t', 'T', 'T'},
	{'y', 'Y', 'Y'},
	{'u', 'U', 'U'},
	{'i', 'I', 'I'},
	{'o', 'O', 'O'},
	{'p', 'P', 'P'},
	{'[', '{', '['},
	{']', '}', ']'},
	{'\n', '\n', '\n'},
	{'\0', '\0', '\0'},
	{'a', 'A', 'A'},
	{'s', 'S', 'S'},
	{'d', 'D', 'D'},
	{'f', 'F', 'F'},
	{'g', 'G', 'G'},
	{'h', 'H', 'H'},
	{'j', 'J', 'J'},
	{'k', 'K', 'K'},
	{'l', 'L', 'L'},
	{';', ':', ';'},
	{'\'', '"', '\''},
	{'`', '~', '`'},
	{'\0', '\0', '\0'},
	{'\\', '|', '\\'},
	{'z', 'Z', 'Z'},
	{'x', 'X', 'X'},
	{'c', 'C', 'C'},
	{'v', 'V', 'V'},
	{'b', 'B', 'B'},
	{'n', 'N', 'N'},
	{'m', 'M', 'M'},
	{',', '<', ','},
	{'.', '>', '.'},
	{'/', '?', '/'},
	{'\0', '\0', '\0'},
	{'*', '*', '*'},
	{'\0', '\0', '\0'},
	{' ', ' ', ' '},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'},
	{'7', '7', '7'},
	{'8', '8', '8'},
	{'9', '9', '9'},
	{'-', '-', '-'},
	{'4', '4', '4'},
	{'5', '5', '5'},
	{'6', '6', '6'},
	{'+', '+', '+'},
	{'1', '1', '1'},
	{'2', '2', '2'},
	{'3', '3', '3'},
	{'0', '0', '0'},
	{'.', '.', '.'},
	{'\0', '\0', '\0'},
	{'\0', '\0', '\0'}
};

char shift_state = 0;
char caps_state = 0;

void *kb_evfunc[KB_EVFUNC_COUNT] = {0};

int register_kb_evfunc(void *id){
	for(int i = 0; i < KB_EVFUNC_COUNT; i++){
		if(kb_evfunc[i] == 0){
			kb_evfunc[i] = id;
			return i;
		}
	}
	return -1;
}

int unregister_kb_evfunc(void *id){
	for(int i = 0; i < KB_EVFUNC_COUNT; i++){
		if(kb_evfunc[i] == id){
			kb_evfunc[i] = 0;
			return i;
		}
	}
	return -1;
}

void call_kb_evfunc(uint8_t scancode){
	for(int i = 0; i < KB_EVFUNC_COUNT; i++){
		if(kb_evfunc[i] != 0){
			void (*kb_event)(uint8_t) = kb_evfunc[i];
			kb_event(scancode);
		}
	}
}

char get_value(char scancode){
	if(caps_state){
		if(shift_state){
			return scancode_set1[(int)scancode].c;
		}
		return scancode_set1[(int)scancode].caps;
	}
	else if(shift_state){
		return scancode_set1[(int)scancode].shift;
	}
	else{
		return scancode_set1[(int)scancode].c;
	}
}
