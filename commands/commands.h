#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#include "../terminal/terminal.h"
#include "stdio.h"

void call_command(const char *command);
void clear(void);
void no_u(void);

#endif /*__COMMANDS_H__*/
