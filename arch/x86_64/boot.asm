%include "macro.asm"
%include "gdt.asm"
%include "idt.asm"

;align loaded modules on page boundaries
MEMALIGN equ 1<<0
;provide memory map
MEMINFO equ 1<<1
;this is the Multiboot 'flag' field
FLAGS equ MEMALIGN | MEMINFO
;'magic number' lets bootloader find the header
MAGIC equ 0x1BADB002
;checksum of above, to prove we are multiboot
CHECKSUM equ -(MAGIC + FLAGS)

section .multiboot
align 4
	dd MAGIC
	dd FLAGS
	dd CHECKSUM

section .bss
align 16
stack_bottom:
resb 0x4000 ;16 KiB
stack_top:

section .text
global _start
_start:
	mov esp, stack_top
	cli

	pic_remap 0x20, 0x28

	lgdt [gdtr]

	push 0x8
	mov rax, loadcs
	push rax
	retf
	loadcs:
		mov ax, 0x10
		mov ds, ax
		mov es, ax
		mov fs, ax
		mov gs, ax
		mov ss, ax

	init_idt

	lidt [idtr]

	mov eax, 1
	cpuid
	test eax, 1<<25
	jz .nosse
	;enable see
	mov eax, cr0
	or ax, 0x2
	mov cr0, eax
	mov eax, cr4
	or ax, 3 << 9
	mov cr4, eax
	.nosse:
	;enable paging and protection
	mov eax, cr0
	or eax, 0x80000001
	mov cr0, eax

	sti

	extern kernel_main
	call kernel_main

	;If the system has nothing more to do, put the computer into an
	;infinite loop. To do that:
	;1) Disable interrupts with cli (clear interrupt enable in eflags).
	;   They are already disabled by the bootloader, so this is not needed.
	;   Mind that you might later enable interrupts and return from
	;   kernel_main (which is sort of nonsensical to do).
	;2) Wait for the next interrupt to arrive with hlt (halt instruction).
	;   Since they are disabled, this will lock up the computer.
	;3) Jump to the hlt instruction if it ever wakes up due to a
	;   non-maskable interrupt occurring or due to system management mode.
	cli
.hang:
	hlt
	jmp .hang
