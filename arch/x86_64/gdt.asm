global gdtr
section .rodata
align 16
gdt_table:
	;gdt null segment
	dw 0x0 ; limit
	dw 0x0 ; base
	db 0x0 ; base
	db 0x0 ; access
	db 0x0 ; flags (4:7) & limit (0:3)
	db 0x0 ; base

	;gdt executable segment:
	dw 0xFF ; limit
	dw 0x0 ; base
	db 0x0 ; base
	db 0b10011010 ; access
	db 0b10101111 ; flags (4:7) & limit (0:3)
	db 0x0 ; base

	;gdt read write segment
	dw 0xFF ; limit
	dw 0x0 ; base
	db 0x0 ; base
	db 0b10010010 ; access
	db 0b10101111 ; flags (4:7) & limit (0:3)
	db 0x0 ; base

gdtr:
	dw 0x17 ;size in bytes
	dd gdt_table ;address of the table
