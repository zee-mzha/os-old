section .data
last_event_push: dw 0
last_event_grab: dw 0

section .bss
event_queue:
resb 0x400

section .text

;arguments: one byte scancode
;return: none
global push_event
push_event:
	cmp word [last_event_push], 1024
	jne .continue
	mov word [last_event_push], 0
	.continue:
	mov edx, 0
	mov dx, [last_event_push]
	mov al, [esp+4]
	mov byte [event_queue+edx], al
	inc word [last_event_push]
	ret

;arguments: none
;return: ony byte scancode
;return 0 indicates no event
global grab_event
grab_event:
	cmp word [last_event_grab], 1024
	jne .continue
	mov word [last_event_grab], 0
	.continue:
	mov edx, 0
	mov dx, [last_event_grab]
	mov al, [event_queue+edx]
	cmp al, 0
	je .no_event
	inc word [last_event_grab]
	.no_event:
	ret
