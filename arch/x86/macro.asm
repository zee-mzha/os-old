%ifndef __MACRO_S__
%define __MACRO_S__

%macro pic_remap 2;PIC1_offset, PIC2_offset
	;save masks into dl and dh
	push ax
	in al, PIC1_DATA
	mov dl, al
	in al, PIC2_DATA
	mov dh, al
	mov al, 0x11

	;begin initialization
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al

	;set vector offsets
	mov al, %1
	out PIC1_DATA, al
	mov al, %2
	out PIC2_DATA, al

	;tell master PIC that slave PIC is at IRQ2
	mov al, 0x4
	out PIC1_DATA, al
	;tell slave PIC its cascade identity
	mov al, 0x20
	out PIC2_DATA, al

	;put in 8086 mode, whatever that means
	mov al, 0x1
	out PIC1_DATA, al
	out PIC2_DATA, al

	;restore masks
	mov al, dl
	out PIC1_DATA, al
	mov al, dh
	out PIC2_DATA, al
	pop ax
%endmacro

%endif ;__MACRO_S__
