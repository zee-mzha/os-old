%define PIC1_COMMAND 0x20
%define PIC1_DATA 0x21
%define PIC2_COMMAND 0xA0
%define PIC2_DATA 0xA1
%define PIC_EOI 0x20

section .rodata
idtr:
	dw 0x17F ;size in bytes
	dd idt_table ;address of the table

section .bss
;since we're just allocating memory this should technically be here
idt_table:
resb 0x180 ;384 bytes, 48 entries

%macro make_idt_entry 4;index, address, segment, type
	push edx

	;move address into table
	mov edx, %2
	mov [idt_table+(%1)*8], dx
	shr edx, 16
	mov [idt_table+(%1)*8+6], dx

	;move segment into table
	mov dx, %3
	mov [idt_table+(%1)*8+2], dx

	;move type and 0 byte into table
	mov dl, 0x0
	mov dh, %4
	mov [idt_table+(%1)*8+4], dx

	pop edx
%endmacro

%macro init_idt 0
	make_idt_entry 0, int_0, 0x8, 0x8E
	make_idt_entry 1, int_1, 0x8, 0x8E
	make_idt_entry 2, int_2, 0x8, 0x8E
	make_idt_entry 3, int_3, 0x8, 0x8E
	make_idt_entry 4, int_4, 0x8, 0x8E
	make_idt_entry 5, int_5, 0x8, 0x8E
	make_idt_entry 6, int_6, 0x8, 0x8E
	make_idt_entry 7, int_7, 0x8, 0x8E
	make_idt_entry 8, int_8, 0x8, 0x8E
	make_idt_entry 9, int_9, 0x8, 0x8E
	make_idt_entry 10, int_10, 0x8, 0x8E
	make_idt_entry 11, int_11, 0x8, 0x8E
	make_idt_entry 12, int_12, 0x8, 0x8E
	make_idt_entry 13, int_13, 0x8, 0x8E
	make_idt_entry 14, int_14, 0x8, 0x8E
	make_idt_entry 15, int_15, 0x8, 0x8E
	make_idt_entry 16, int_16, 0x8, 0x8E
	make_idt_entry 17, int_17, 0x8, 0x8E
	make_idt_entry 18, int_18, 0x8, 0x8E
	make_idt_entry 19, int_19, 0x8, 0x8E
	make_idt_entry 20, int_20, 0x8, 0x8E
	make_idt_entry 21, int_21, 0x8, 0x8E
	make_idt_entry 22, int_22, 0x8, 0x8E
	make_idt_entry 23, int_23, 0x8, 0x8E
	make_idt_entry 24, int_24, 0x8, 0x8E
	make_idt_entry 25, int_25, 0x8, 0x8E
	make_idt_entry 26, int_26, 0x8, 0x8E
	make_idt_entry 27, int_27, 0x8, 0x8E
	make_idt_entry 28, int_28, 0x8, 0x8E
	make_idt_entry 29, int_29, 0x8, 0x8E
	make_idt_entry 30, int_30, 0x8, 0x8E
	make_idt_entry 31, int_31, 0x8, 0x8E
	make_idt_entry 32, int_32, 0x8, 0x8E
	make_idt_entry 33, int_33, 0x8, 0x8E
	make_idt_entry 34, int_34, 0x8, 0x8E
	make_idt_entry 35, int_35, 0x8, 0x8E
	make_idt_entry 36, int_36, 0x8, 0x8E
	make_idt_entry 37, int_37, 0x8, 0x8E
	make_idt_entry 38, int_38, 0x8, 0x8E
	make_idt_entry 39, int_39, 0x8, 0x8E
	make_idt_entry 40, int_40, 0x8, 0x8E
	make_idt_entry 41, int_41, 0x8, 0x8E
	make_idt_entry 42, int_42, 0x8, 0x8E
	make_idt_entry 43, int_43, 0x8, 0x8E
	make_idt_entry 44, int_44, 0x8, 0x8E
	make_idt_entry 45, int_45, 0x8, 0x8E
	make_idt_entry 46, int_46, 0x8, 0x8E
	make_idt_entry 47, int_47, 0x8, 0x8E
%endmacro

section .text

extern push_event

int_0:
	iretd

int_1:
	iretd

int_2:
	iretd

int_3:
	iretd

int_4:
	iretd

int_5:
	iretd

int_6:
	iretd

int_7:
	iretd

int_8:
	iretd

int_9:
	iretd

int_10:
	iretd

int_11:
	iretd

int_12:
	iretd

int_13:
	iretd

int_14:
	iretd

int_15:
	iretd

int_16:
	iretd

int_17:
	iretd

int_18:
	iretd

int_19:
	iretd

int_20:
	iretd

int_21:
	iretd

int_22:
	iretd

int_23:
	iretd

int_24:
	iretd

int_25:
	iretd

int_26:
	iretd

int_27:
	iretd

int_28:
	iretd

int_29:
	iretd

int_30:
	iretd

int_31:
	iretd

int_32:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_33:
	push eax
	mov eax, 0
	in al, 0x60
	push eax
	call push_event
	add esp, 4
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_34:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_35:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_36:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_37:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_38:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_39:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	pop eax
	iretd

int_40:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_41:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_42:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_43:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_44:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_45:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_46:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd

int_47:
	push eax
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	out PIC2_COMMAND, al
	pop eax
	iretd
