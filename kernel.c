#include "terminal/terminal.h"
#include "keyboard/keyboard.h"
#include "commands/commands.h"
#include "string.h"
#include "stdio.h"

extern uint8_t grab_event(void);
void kb_event(uint8_t event);

void kernel_main(void){
	char event;
	register_kb_evfunc(kb_event);
	
	clear();
	cursor_pos = 38; //center
	puts("~os~\n");
	printf("> ");
	while(1){
		asm volatile("hlt");
		event = grab_event();
		if(event != 0){
			call_kb_evfunc(event);
		}
	}
}

void kb_event(uint8_t event){
	static char command_buffer[VGA_WIDTH*VGA_HEIGHT];
	static uint16_t command_pointer;

	/*shift and caps lock handling*/
	if(event == 0x3A){
		caps_state = 1;
	}
	else if(event == 0xBA){
		caps_state = 0;
	}
	else if(event == 0x36 || event == 0x2A){
		shift_state = 1;
	}
	else if(event == 0xB6 || event == 0xAA){
		shift_state = 0;
	}
	else if(event < 0x59){
		if(get_value(event) != '\0'){
			if(get_value(event) == '\b'){
				if(command_pointer != 0){
					command_pointer--;
					putchar('\b');
					command_buffer[command_pointer] = '\0';
				}
			}
			else if(get_value(event) == '\n'){
				putchar('\n');
				call_command(command_buffer);
				printf("> ");
				command_pointer = 0;
				memset(command_buffer, 0, sizeof(command_buffer));
			}
			else{
				command_buffer[command_pointer] = get_value(event);
				putchar(get_value(event));
				command_pointer++;
			}
		}
	}
}
