#ifndef __STDIO_H__
#define __STDIO_H__

#include <stdarg.h>
#include <stddef.h>
#include "../terminal/terminal.h"

int putchar(char c);
int puts(const char *str);
int printf(const char *format, ...);

#endif /*__STDIO_H__*/
