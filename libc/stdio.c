#include "stdio.h"

int putchar(char c){
	terminal_putchar(c);
	return c;
}

int puts(const char *str){
	terminal_puts(str);
	terminal_putchar('\n');
	return 0;
}

int printf(const char *format, ...){
	va_list ap;
	va_start(ap, format);
	for(int i = 0; ; i++){
		if(format[i] == '\0'){
			break;
		}
		else{
			terminal_putchar(format[i]);
		}
	}
	va_end(ap);
	return 0;
}
