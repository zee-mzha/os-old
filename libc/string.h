#ifndef __STRING_H__
#define __STRING_H__

#include <stddef.h>

size_t strlen(const char *);
void *memset(void *, int, size_t);
void *memcpy(void *, void *, size_t);
int strcmp(const char *, const char *);
int strncmp(const char *, const char *, size_t);

#endif /*__STRING_H__*/

