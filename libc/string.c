#include "string.h"

size_t strlen(const char *str){
	for(size_t i = 0;; i++){
		if(str[i] == '\0'){
			return i;
		}
	}
}
void *memset(void *ptr, int value, size_t num){
	for(size_t i = 0; i < num; i++){
		((char*)ptr)[i] = value;
	}
	return ptr;
}
void *memcpy(void *dest, void *src, size_t count){
	for(size_t i = 0; i < count; i++){
		((char*)dest)[i] = ((char*)src)[i];
	}
	return dest;
}

int strcmp(const char *str1, const char *str2){
	for(size_t i = 0; ; i++){
		if(str1[i] != str2[i]){
			return str1[i] - str2[i];
		}
		if(str1[i] == '\0'){
			return 0;
		}
	}
}

int strncmp(const char *str1, const char *str2, size_t count){
	for(size_t i = 0; i < count; i++){
		if(str1[i] != str2[i]){
			return str1[i] - str2[i];
		}
		if(str1[i] == '\0'){
			return 0;
		}
	}
	return 0;
}

